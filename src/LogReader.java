import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.DecimalFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class LogReader {

    private List<File> listOfLogFiles;
    private File directoryWhereSearchingStarts;
    private String directoryNameToSearch;
    private boolean isLogsDirectoryFound = false;

    public LogReader(String pathToSearchStartingDirectory, String directoryName) {
        directoryWhereSearchingStarts = new File(pathToSearchStartingDirectory);
        this.directoryNameToSearch = directoryName;
        this.listOfLogFiles = new ArrayList<>();
    }

    public void findInterpretPrintLogs() {
        listOfLogFiles = searchDirectoryWithLogs(directoryWhereSearchingStarts);
        if (listOfLogFiles.isEmpty()) {
            if (!isLogsDirectoryFound) {
                System.out.println("Directory with logs has not been found, aborting");
                return;
            } else {
                System.out.println("Directory has been found but there is not any log files, aborting");
                return;
            }
        }
        listOfLogFiles.forEach(fileWithLogs -> {
                    System.out.println("-----------------------");
                    System.out.println("Interpreting file " + fileWithLogs.getName());
                    System.out.println();
                    double startPointForReadingFile = System.nanoTime();
                    List<String> listOfLogStrings = getLogsAsListOfStrings(fileWithLogs);
                    if (listOfLogStrings.isEmpty()) {
                        System.out.println("File " + fileWithLogs.getName() + "does not contain any logs");
                        return;
                    }
                    System.out.println("Library Occurs:");
                    printUniqueLibraryOccurs(countUniqueLibraryOccurs(listOfLogStrings));
                    System.out.println();
                    System.out.println("Logs Severity:");
                    countAndPrintLogsSeverity(listOfLogStrings);
                    System.out.println();
                    printTimeRangeOfLogs(listOfLogStrings);
                    double endPointForReadingFile = System.nanoTime();
                    System.out.println();
                    System.out.println("File has been read in: " +
                            (endPointForReadingFile - startPointForReadingFile) / 1000000000 + " s");
                }
        );
    }

    private List<File> searchDirectoryWithLogs(File targetDirectoryWithLogs) {

        if (targetDirectoryWithLogs.isDirectory() && targetDirectoryWithLogs.getName().equals(directoryNameToSearch)) {
            System.out.println("Directory has been found, checking files...");
            Arrays.asList(Objects.requireNonNull(targetDirectoryWithLogs.listFiles())).forEach(fileInsideLogDirectory -> {
                        if (checkIfFileIsLogExtension(fileInsideLogDirectory)) {
                            System.out.println("File " + fileInsideLogDirectory.getName() + " is log file. Copying to list");
                            listOfLogFiles.add(fileInsideLogDirectory);
                        } else {
                            System.out.println("File " + fileInsideLogDirectory.getName() + " is not a log file, so it was skipped ");
                        }
                    }
            );
            isLogsDirectoryFound = true;
        } else if (targetDirectoryWithLogs.isDirectory()) {
            System.out.println("Searching directory ..." + targetDirectoryWithLogs.getAbsoluteFile());
            if (targetDirectoryWithLogs.listFiles()==null){
                System.out.println();
            }else{
                for (File temp : Objects.requireNonNull(targetDirectoryWithLogs.listFiles())) {
                    if (isLogsDirectoryFound) {
                        sortFilesByLastModification(listOfLogFiles);
                        return sortFilesByLastModification(listOfLogFiles);
                    }
                    if (temp.isDirectory()) {
                        searchDirectoryWithLogs(temp);
                    }
                }
            }
        } else {
            System.out.println(targetDirectoryWithLogs.getAbsoluteFile() + "Permission Denied");
        }
        return listOfLogFiles;
    }

    private List<String> getLogsAsListOfStrings(File logFile) {
        List<String> lines;
        List<String> separateLogsList = new ArrayList<>();
        try {
            lines = Files.readAllLines(logFile.toPath());
        } catch (IOException exception) {
            System.out.println(exception + "File cannot be read");
            return separateLogsList;
        }
        for (String line : lines) {
            if (line.trim().isEmpty()) {
                continue;
            }
            if (checkIfStringIsDateFormat(line)) {
                separateLogsList.add(line);
            } else if (!separateLogsList.isEmpty()) {
                String joined = new StringBuilder().append(separateLogsList.get(separateLogsList.size() - 1))
                        .append("\n ")
                        .append(line)
                        .toString();
                separateLogsList.set(separateLogsList.size() - 1, joined);
            }
        }
        return separateLogsList;
    }

    private Map<String, Integer> countUniqueLibraryOccurs(List<String> logList) {
        Map<String, Integer> uniqueLibraryOccursMap = new HashMap<>();
        for (String temp : logList) {
            temp = temp.substring(temp.indexOf("[") + 1, temp.indexOf("]"));
            if (uniqueLibraryOccursMap.containsKey(temp)) {
                uniqueLibraryOccursMap.computeIfPresent(temp, (key, value) -> value + 1);

            } else {
                uniqueLibraryOccursMap.put(temp, 1);
            }
        }
        return uniqueLibraryOccursMap;
    }

    private void countAndPrintLogsSeverity(List<String> logList) {
        int fatalCounter = 0;
        int errorCounter = 0;
        int warningCounter = 0;
        int infoCounter = 0;
        int debugCounter = 0;
        int traceCounter = 0;

        for (String log : logList) {
            if (log.contains("FATAL")) {
                fatalCounter++;
            }
            if (log.contains("ERROR")) {
                errorCounter++;
            }
            if (log.contains("WARNING")) {
                warningCounter++;
            }
            if (log.contains("INFO")) {
                infoCounter++;
            }
            if (log.contains("DEBUG")) {
                debugCounter++;
            }
            if (log.contains("TRACE")) {
                traceCounter++;
            }
        }
        System.out.println("File contains following amount of Logs: " + logList.size());
        System.out.println("Including:");
        System.out.println(fatalCounter + " Fatal Severity");
        System.out.println(errorCounter + " Error Severity");
        System.out.println(warningCounter + " Warning Severity");
        System.out.println(infoCounter + " Info Severity");
        System.out.println(debugCounter + " Debug Severity");
        System.out.println(traceCounter + " Trace Severity");

        int amountOfErrorLogsAndHigher = errorCounter + fatalCounter;
        double ratio = (double) amountOfErrorLogsAndHigher / (double) logList.size();
        DecimalFormat fourAfterComaFormat = new DecimalFormat("0.0000");
        System.out.println("There are " + amountOfErrorLogsAndHigher + " logs with severity ERROR and higher for " +
                logList.size() + " logs");
        System.out.println("ERROR and higher for ratio to all is: " + fourAfterComaFormat.format(ratio));
    }

    private void printUniqueLibraryOccurs(Map<String, Integer> mapOfLibraryOccurs) {
        mapOfLibraryOccurs.forEach((key, value) -> System.out.println(value + " logs with " + "[" + key + "]" + " library"));
    }

    private void printTimeRangeOfLogs(List<String> logList) {
        List<LocalDateTime> logsAsLocalDateTimeList = sortLogsFromEarliest(logList);
        if (logsAsLocalDateTimeList.isEmpty()) {
            System.out.println("File does not contain any logs");
            return;
        }
        String message;

        LocalDateTime localDateTimeStart = logsAsLocalDateTimeList.get(0);
        LocalDateTime localDateTimeEnd = logsAsLocalDateTimeList.get(logsAsLocalDateTimeList.size() - 1);

        LocalDate localDateStart = localDateTimeStart.toLocalDate();
        LocalDate localDateEnd = localDateTimeEnd.toLocalDate();

        LocalTime localTimeStart = localDateTimeStart.toLocalTime();
        LocalTime localTimeEnd = localDateTimeEnd.toLocalTime();

        Period periodBetweenDate;

        Duration durationBetweenHours;

        if (localTimeStart.compareTo(localTimeEnd) > 0) {
            periodBetweenDate = Period.between(localDateStart, localDateEnd.minusDays(1));

            durationBetweenHours = Duration.between(localTimeStart, localTimeEnd).plusHours(24);
        } else {
            periodBetweenDate = Period.between(localDateStart, localDateEnd);
            durationBetweenHours = Duration.between(localTimeStart, localTimeEnd);
        }
        String duration = durationBetweenHours.toString();

        if (!duration.contains("H")) {
            duration = new StringBuilder()
                    .append(duration, 0, 2)
                    .append("0H")
                    .append(duration, duration.indexOf("T") + 1, duration.length())
                    .toString();
        }
        if (!duration.contains("M")) {
            duration = new StringBuilder()
                    .append(duration, 0, duration.indexOf("H") + 1)
                    .append("0M")
                    .append(duration, duration.indexOf("H") + 1, duration.length())
                    .toString();
        }
        if (!duration.contains("S")) {
            duration = new StringBuilder()
                    .append(duration)
                    .append("0S")
                    .toString();
        }
        message = new StringBuilder().append("Time range for file is:")
                .append("\n")
                .append(periodBetweenDate.getYears()).append(" years ")
                .append(periodBetweenDate.getMonths()).append(" months ")
                .append(periodBetweenDate.getDays()).append(" days ")
                .append("\n")
                .append(duration, duration.indexOf("T") + 1, duration.indexOf("H")).append(" hours ")
                .append(duration, duration.indexOf("H") + 1, duration.indexOf("M")).append(" minutes ")
                .append(duration, duration.indexOf("M") + 1, duration.length() - 1).append(" seconds ")
                .toString();
        System.out.println(message);
    }

    private boolean checkIfFileIsLogExtension(File file) {
        return file.getName().substring(file.getName().length() - 4).equals(".log");
    }

    private boolean checkIfStringIsDateFormat(String fileLine) {
        try {
            LocalDate.parse(fileLine.substring(0, 10), DateTimeFormatter.ISO_LOCAL_DATE);
            return true;
        } catch (DateTimeException dateTimeException) {
            return false;
        }
    }

    private List<File> sortFilesByLastModification(List<File> listOfFiles) {
        listOfFiles.sort(Comparator.comparingLong(File::lastModified));
        Collections.reverse(listOfFiles);
        return listOfFiles;
    }

    private List<LocalDateTime> sortLogsFromEarliest(List<String> logList) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss,SSS");
        LocalDateTime localDateTimeFromString;
        List<LocalDateTime> localDateTimeListForLogsList = new ArrayList<>();
        for (String log : logList) {
            log = log.substring(0, 23);
            try {
                localDateTimeFromString = LocalDateTime.parse(log, dateTimeFormatter);
            } catch (DateTimeException dateTimeException) {
                continue;
            }
            localDateTimeListForLogsList.add(localDateTimeFromString);
        }
        localDateTimeListForLogsList.sort(LocalDateTime::compareTo);
        return localDateTimeListForLogsList;
    }
}
